import java.io.*;
import java.net.*;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.net.*;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class Client {
	public static boolean TEST = false;

	private static Object[] unpackKeyAndIV(byte[] data) {
		return new Object[] { new SecretKeySpec(data, 0, 16, "AES"),
				new IvParameterSpec(data, 16, data.length - 16) };
	}

	public static void main(String argv[]) throws Exception {
		String sentence = "";
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(
				System.in));
		Socket clientSocket = new Socket("localhost", 6790);
		DataOutputStream outToServer = new DataOutputStream(
				clientSocket.getOutputStream());
		DataInputStream inFromServer = new DataInputStream(
				clientSocket.getInputStream());

		// Add in a securityprovider
		Security.addProvider(new BouncyCastleProvider());
		SecureRandom random = Utils.createFixedRandom();

		// read the server public key from the public.key file
		PublicKey serverPubKey = readPublicKeyFromFile();

		// Generate a 512 bit Private and Public RSA key for the client
		// create the RSA Key
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");

		generator.initialize(512, random);

		KeyPair pair = generator.generateKeyPair();
		Key pubKey = pair.getPublic();
		Key privKey = pair.getPrivate();

		// send your public key to the server encrypted with the servers public
		// key
		Cipher xCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");

		xCipher.init(Cipher.ENCRYPT_MODE, serverPubKey, random);

		byte[] keyBlock = xCipher.doFinal(pubKey.getEncoded());

		if (TEST) {
			System.out.println("Sent public key: "
					+ Utils.toHex(pubKey.getEncoded()));
			System.out.println("Encrypted to server: " + Utils.toHex(keyBlock));
		}
		outToServer.write(keyBlock);

		// Receive the new AES symmetric key from the server
		// use the unpackKeyAndIV object to regenerate the key and IV
		byte[] aesMessage = new byte[64];
		int r = inFromServer.read(aesMessage);
		if (TEST)
			System.out.println("Size of aesMessage: " + r);
		xCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");
		xCipher.init(Cipher.DECRYPT_MODE, privKey);
		byte[] decryptedKeyIV = new byte[512];
		decryptedKeyIV = xCipher.update(aesMessage);
		decryptedKeyIV = xCipher.doFinal();
		Object[] keyIV = unpackKeyAndIV(decryptedKeyIV);

		// Read input from console, excrypt it using key and IV and
		// send to the server
		System.out.print("Write Something here->");
		sentence = inFromUser.readLine();
		Cipher sCipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");

		sCipher.init(Cipher.ENCRYPT_MODE, (Key) keyIV[0],
				(IvParameterSpec) keyIV[1]);

		byte[] cipherText = sCipher.update(sentence.getBytes());
		cipherText = sCipher.doFinal();
		System.out.println("Sending: " + sentence);
		System.out.println("encrypted as: " + Utils.toHex(cipherText));
		outToServer.write(cipherText);

		// Decrypt response from server and print in decrypted and encypted
		// formats.
		byte[] serverResponse = new byte[512];
		r = inFromServer.read(serverResponse);

		sCipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");
		sCipher.init(Cipher.DECRYPT_MODE, (Key) keyIV[0],
				(IvParameterSpec) keyIV[1]);
		byte[] serverDecrypted = sCipher.update(serverResponse, 0, r);
		serverDecrypted = sCipher.doFinal();
		System.out.println("Server sent: " + Utils.toHex(serverResponse));
		System.out.println("Decrypted as: " + new String(serverDecrypted));

		while (!sentence.contentEquals("quit")) {

			// use the symmetric key to encrypt and decrypt messages to the
			// sever
			sentence = inFromUser.readLine();

			sCipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");

			sCipher.init(Cipher.ENCRYPT_MODE, (Key) keyIV[0],
					(IvParameterSpec) keyIV[1]);

			cipherText = sCipher.doFinal(sentence.getBytes());

			outToServer.writeUTF(cipherText.toString() + '\n');

			System.out.println("FROM SERVER: " + inFromServer.readUTF());
			System.out.print("Write Something here->");
		}
		clientSocket.close();
	}

	private static PublicKey readPublicKeyFromFile() throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		File filePublicKey = new File("public.key");
		FileInputStream fis = new FileInputStream("public.key");
		byte[] encodedPublicKey = new byte[(int) filePublicKey.length()];
		fis.read(encodedPublicKey);
		fis.close();

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
				encodedPublicKey);
		PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

		if (TEST)
			System.out.println("PublicKey: " + publicKey.toString());

		return publicKey;
	}
}

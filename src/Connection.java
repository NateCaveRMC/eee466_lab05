import javax.crypto.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.spec.*;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Connection extends Thread {
	public final static boolean TEST = false;
	String clientSentence;
	String capitalizedSentence;
	DataInputStream inFromClient;
	DataOutputStream outToClient;
	Socket connSocket;
	int id;

	Connection(Socket s, int num) {
		// add a new security provider
		connSocket = s;
		id = num;
		System.out.println("I'm Thread: " + id);

		try {
			inFromClient = new DataInputStream(connSocket.getInputStream());
			outToClient = new DataOutputStream(connSocket.getOutputStream());
			this.start();

		} catch (Exception e) {

		}
	}

	private static byte[] packKeyAndIv(Key key, IvParameterSpec ivSpec)
			throws IOException {
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		bOut.write(key.getEncoded());
		bOut.write(ivSpec.getIV());

		return bOut.toByteArray();
	}

	public void run() {
		try {
			Security.addProvider(new BouncyCastleProvider());
			System.out.println("Running connection...");

			// Read the private key from the file
			if (TEST)
				System.out.println("Reading private key from file...");
			PrivateKey privateKey = readPrivateKeyFromFile();
			if (TEST)
				System.out.println("Private key read."
						+ Utils.toHex(privateKey.getEncoded()));

			// get the public key from the client
			if (TEST)
				System.out.println("Reading public key from client...");
			PublicKey clientPublicKey;
			byte[] clientEncryptedMessage = new byte[512];
			int r = inFromClient.read(clientEncryptedMessage);
			byte[] clientDecryptedMessage = new byte[r];
			if (TEST)
				System.out.println("Received message: "
						+ Utils.toHex(clientEncryptedMessage));

			Cipher sCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");

			sCipher.init(Cipher.DECRYPT_MODE, privateKey);
			clientDecryptedMessage = sCipher.update(clientEncryptedMessage, 0,
					r);
			clientDecryptedMessage = sCipher.doFinal();
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
					clientDecryptedMessage);
			clientPublicKey = keyFactory.generatePublic(publicKeySpec);

			if (TEST)
				System.out.println("clientPublicKey decrypted: "
						+ Utils.toHex(clientPublicKey.getEncoded()));

			// generate a new AES symmetric key using AES 128 bit encryption and
			// an IV
			SecureRandom random = Utils.createFixedRandom();
			Key sKey = Utils.createKeyForAES(128, random);
			IvParameterSpec sIvSpec = Utils.createCtrIvForAES(0, random);

			// send the packedKeyAndIv to the client encrypted with the clients
			// public key
			sCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");
			sCipher.init(Cipher.ENCRYPT_MODE, clientPublicKey, random);
			// package the Key and IV using the packKeyAndIv() method
			byte[] packedKeyAndIv = sCipher
					.doFinal(packKeyAndIv(sKey, sIvSpec));
			outToClient.write(packedKeyAndIv);

			// receive message encrypted with new session key
			byte[] clientMessage = new byte[512];
			r = inFromClient.read(clientMessage);
			byte[] plainText = new byte[r];
			sCipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");
			sCipher.init(Cipher.DECRYPT_MODE, sKey, sIvSpec);
			plainText = sCipher.update(clientMessage, 0, r);
			plainText = sCipher.doFinal();

			// print in encypted and decrypted formats
			System.out.println("Client sent: " + new String(plainText));
			System.out.println("Encrypted as: " + Utils.toHex(clientMessage));

			// capitalize and encrypt. Send back to client
			String capitalized = new String(plainText).toUpperCase();
			sCipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");
			sCipher.init(Cipher.ENCRYPT_MODE, sKey, sIvSpec);
			byte[] response = new byte[r];
			response = sCipher.update(capitalized.getBytes(), 0, r);
			response = sCipher.doFinal();
			System.out.println("Sending response: " + capitalized);
			System.out.println("Encrypted as: " + Utils.toHex(response));
			outToClient.write(response);

		} catch (Exception e) {

		}
	}

	private static PrivateKey readPrivateKeyFromFile() throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		File filePrivateKey = new File("private.key");
		FileInputStream fis = new FileInputStream("private.key");
		byte[] encodedPrivateKey = new byte[(int) filePrivateKey.length()];
		fis.read(encodedPrivateKey);
		fis.close();

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(
				encodedPrivateKey);
		PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

		if (TEST)
			System.out.println("PrivateKey: "
					+ Utils.toHex(privateKey.getEncoded()));

		return privateKey;
	}

}

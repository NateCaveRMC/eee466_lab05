////////////////////////////////////////////////////
/*Originaly Created by David Hook (dgh@bund.com.au) for Beginning Cryptography with Java
 * http://ca.wiley.com/WileyCDA/WileyTitle/productCd-0764596330.html
 * This is an example piece of code that highlights the various
 * aspects of how to generate and pack an AES key using a public
 * key. Use this as an example for doing the encryption
 */
////////////////////////////////////////////////////
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * RSA example with OAEP Padding and random key generation.
 */
public class RSAKeyExchangeExample {
	private static byte[] packKeyAndIv(Key key, IvParameterSpec ivSpec)
			throws IOException {
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		bOut.write(key.getEncoded());
		bOut.write(ivSpec.getIV());

		return bOut.toByteArray();
	}

	private static Object[] unpackKeyAndIV(byte[] data) {
		return new Object[] { new SecretKeySpec(data, 0, 16, "AES"),
				new IvParameterSpec(data, 16, data.length - 16),

		};
	}

	public static void main(String[] args) throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		byte[] input = new byte[] { 0x00, (byte) 0xbe, (byte) 0xef };
		SecureRandom random = Utils.createFixedRandom();

		// create the RSA Key
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");

		generator.initialize(512, random);

		KeyPair pair = generator.generateKeyPair();
		Key pubKey = pair.getPublic();
		Key privKey = pair.getPrivate();

		System.out.println("input            : " + Utils.toHex(input));

		// create the symmetric key and iv
		Key sKey = Utils.createKeyForAES(128, random);
		IvParameterSpec sIvSpec = Utils.createCtrIvForAES(0, random);

		// symmetric key/iv wrapping step
		Cipher xCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");

		xCipher.init(Cipher.ENCRYPT_MODE, pubKey, random);

		byte[] keyBlock = xCipher.doFinal(packKeyAndIv(sKey, sIvSpec));

		// encryption step
		Cipher sCipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");

		sCipher.init(Cipher.ENCRYPT_MODE, sKey, sIvSpec);

		byte[] cipherText = sCipher.doFinal(input);

		System.out.println("keyBlock length  : " + keyBlock.length);
		System.out.println("cipherText length: " + cipherText.length);

		// symmetric key/iv unwrapping step
		xCipher.init(Cipher.DECRYPT_MODE, privKey);

		Object[] keyIv = unpackKeyAndIV(xCipher.doFinal(keyBlock));

		// decryption step
		sCipher.init(Cipher.DECRYPT_MODE, (Key) keyIv[0],
				(IvParameterSpec) keyIv[1]);

		byte[] plainText = sCipher.doFinal(cipherText);

		System.out.println("plain            : " + Utils.toHex(plainText));
	}
}